VAGRANT_MEM = 8192
VAGRANT_CPU = 4
VAGRANT_DISK = 100

Vagrant.configure('2') do |config|
  config.vm.box = vagrant_box
  config.vm.synced_folder '.', '/vagrant', disabled: true
  config.vm.synced_folder '.', '/home/vagrant/sync', disabled: true
  config.vm.provider(:libvirt) { |d| configure_libvirt(d) }
  config.vm.provider(:virtualbox) { |v, o| configure_virtualbox(v, o) }
  config.vm.provider(:aws) { |a, o| configure_aws(a, o) }
  config.vm.provision(:ansible) { |a| provision(a) }
end

# KVM configuration
def configure_libvirt(domain)
  domain.memory = VAGRANT_MEM
  domain.cpus = VAGRANT_CPU
  domain.storage :file, size: "#{VAGRANT_DISK}G"
end

# AWS configuration
def configure_aws(aws, override)
  default_keypair_name = [ENV['USER'], ENV['HOSTNAME']].join('@')
  aws.ami = 'ami-42e84f2d'
  aws.region = 'eu-central-1'
  aws.keypair_name = ENV['CI'] ? 'ci@gitlab.com' : default_keypair_name
  aws.block_device_mapping = [
    {
      'DeviceName' => '/dev/sda1',
      'Ebs.DeleteOnTermination' => true
    },
    {
      'DeviceName' => '/dev/sdb',
      'Ebs.DeleteOnTermination' => true,
      'Ebs.VolumeSize' => 100
    }
  ]
  override.vm.box = 'aws'
  override.ssh.username = 'centos'
  override.ssh.private_key_path = '~/.ssh/id_rsa_aws'
end

# VirtualBox configuration
def configure_virtualbox(vbox, override)
  disk_path = './tmp/sdb.vdi'
  vbox.memory = VAGRANT_MEM
  vbox.cpus = VAGRANT_CPU
  unless File.exist?(disk_path)
    vbox.customize [
      'createhd',
      '--filename', disk_path,
      '--size', 1024 * 100
    ]
  end
  vbox.customize [
    'storageattach', :id,
    '--storagectl', 'IDE',
    '--port', 1,
    '--device', 0,
    '--type', 'hdd',
    '--medium', disk_path
  ]
  override.vm.network :forwarded_port, guest: 9090, host: 9090
  override.vm.network :forwarded_port, guest: 8080, host: 8080
end

# Provisioning
def provision(ansible)
  ansible.playbook = 'site.yml'
  ansible.sudo = true
  ansible.groups = {
    ansible_group => ['default']
  }
  ansible.extra_vars = {
    gitlab_runner_registration_token: ENV['GITLAB_CI_TOKEN']
  }
end

def ansible_group
  ENV['VAGRANT_GROUP'] || 'server'
end

def vagrant_box
  ENV['VAGRANT_BOX'] || 'centos/atomic-host'
end
