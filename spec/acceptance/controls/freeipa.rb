control 'freeipa' do
  impact 0.8
  title 'Ensure that the FreeIPA service is running'
  desc '
  Check if the FreeIPA server is reachable.
  '
  tag 'identity', 'freeipa'

  describe command('kubectl describe pod freeipa') do
    its(:exit_status) { should eq 0 }
  end

  describe command('kubectl describe service freeipa') do
    its(:exit_status) { should eq 0 }
  end
end
