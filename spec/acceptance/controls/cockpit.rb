control 'cockpit' do
  impact 0.1
  title 'Ensure service is running'
  desc '
  Ensure that the Cockpit web interface is up and responding.
  '
  tag 'service', 'management'

  describe service('docker') do
    it { should be_running }
  end

  describe command('atomic containers list') do
    its(:stdout) { should include('cockpit/ws') }
  end

  describe http("https://#{ENV['TARGET_HOST']}:9090", ssl_verify: false) do
    its(:status) { should eq 200 }
    its(:body) { should include 'Log in with your server user account.' }
  end
end
