control 'nfsd' do
  impact 1.0
  title 'Ensure that shared files can be accessed'
  desc '
  Check if the NFS server is reachable.
  '
  tag 'fileshare', 'nfs'

  describe command('kubectl describe pod nfsd') do
    its(:exit_status) { should eq 0 }
  end

  describe command('kubectl describe service nfsd') do
    its(:exit_status) { should eq 0 }
  end
end
