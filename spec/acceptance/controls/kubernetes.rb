control 'kubernetes' do
  impact 0.5
  title 'Ensure that the API is responding'
  desc '
  Check if the kubernetes API is responding
  '
  tag 'cluster', 'management'

  [
    'kube-apiserver',
    'kube-scheduler',
    'kube-controller-manager',
    'etcd',
    'kube-proxy',
    'kubelet'
  ].each do |service_name|
    describe service(service_name) do
      it { should be_running }
    end
  end

  describe http("http://#{ENV['TARGET_HOST']}:8080/healthz") do
    its(:status) { should eq 200 }
    its(:body) { should eq 'ok' }
  end

  describe command('kubectl get nodes') do
    its(:stdout) { should match(/(\d+\.){3}\d+\s+\w+/) }
  end
end
