control 'foreman' do
  impact 0.3
  title 'Ensure that the Foreman server is running'
  desc '
  Check if the Foreman server is reachable.
  '
  tag 'management', 'foreman'

  describe command('kubectl describe pod foreman') do
    its(:exit_status) { should eq 0 }
  end

  describe command('kubectl describe service foreman') do
    its(:exit_status) { should eq 0 }
  end
end
