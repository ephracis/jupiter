control 'manageiq' do
  impact 0.5
  title 'Ensure that the ManageIQ service is running'
  desc '
  Check if the ManageIQ service is reachable
  '
  tag 'management', 'manageiq'

  describe command('kubectl describe pod manageiq') do
    its(:exit_status) { should eq 0 }
  end

  describe command('kubectl describe service manageiq') do
    its(:exit_status) { should eq 0 }
  end
end
