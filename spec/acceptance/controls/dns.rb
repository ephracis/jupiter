control 'dns' do
  impact 1.0
  title 'Ensure service is running'
  desc '
  Ensure that the DNS service is running and able to do lookups.
  '
  tag 'service', 'cluster'

  describe command('kubectl --namespace=kube-system describe pod kube-dns') do
    its(:exit_status) { should eq 0 }
  end

  describe host('10.2.0.2', port: 53, protocol: 'tcp') do
    it { should be_reachable }
  end

  describe command('nslookup manageiq.default.svc.jupiter.lan 10.2.0.2') do
    its(:exit_status) { should eq 0 }
  end
end
