# The Jupiter System

The Jupiter System is a fully containerized IT system that we use in our home
to deliver services such as centralized user accounts and filesharing.

## Architecture

The system is based on a Kubernetes cluster running on CentOS Atomic Host. All
services are deployed inside containers running on top of Kubernetes.

    +----------------------------------------+
    |                Services                |
    +----------------------------------------+
    | Fileshare                        (NFS) |
    | Identity                     (FreeIPA) |
    | Device management            (Foreman) |
    | System management           (ManageIQ) |
    +----------------------------------------+
                        |
                        |
    +----------------------------------------+
    |                Cluster                 |
    +----------------------------------------+
    | Orchestration:   Kubernetes            |
    |            OS:   CentOS Atomic Host    |
    +----------------------------------------+

The system is managed by ManageIQ which is fed a system configuration and then
connects to services such as Foreman, Kubernetes and FreeIPA to carry out the
needed changes to the system.

               System configuration
                        |
                        v
                     ManageIQ
                        |
            +-----------+-----------+
            |           |           |
            v           v           v
       Kubernetes    Foreman     FreeIPA
            |           |           |
            v           v           v
       Containers    Devices      Users

The network is divided into three groups: physical devices, services and pods.
Physical devices include servers (cluster nodes), laptops, smartphones and TVs,
while Services and Pods live inside the Kubernetes cluster. It is assumed that
services are on a network that is routable to the cluster nodes.

| Name     | CIDR        |
| -------- | ----------- |
| Devices  | 10.1.0.0/16 |
| Services | 10.2.0.0/16 |
| Pods     | 10.3.0.0/16 |

## Development

### Requirements

The following needs to be installed on the workstation:

- Node
- Ruby
- Ansible
- Vagrant

Check the [Dockerfile of the CI container](containers/ci/Dockerfile) for
details.

### Get started

Install all packages:

```shell
npm install
```

The system can be built inside a virtualized lab for testing and development.
Grunt is used for running common tasks:

```shell
grunt lint              # lint the code
grunt build             # build containers and a virtual lab
grunt build:containers  # build only the containers
grunt build:lab         # build only the lab
grunt test              # run tests against the lab
grunt clean             # remove the lab
```

To access the virtual lab after building it:

```shell
vagrant ssh
```

### Build runners

A custom build runner is used for several reasons. The runner is built with
the following command:

    export GITLAB_CI_TOKEN=<token>
    grunt build:runner

### Release

To release a new version use the following commands:

```shell
# assuming that the current release is 0.0.0
grunt release:major     # bumps to 1.0.0
grunt release:minor     # bumps to 0.1.0
grunt release:patch     # bumps to 0.0.1
grunt release:git       # bumps to 0.0.0-ge96c
grunt release:prepatch  # bumps to 0.0.1-rc.0
grunt release:preminor  # bumps to 0.1.0-rc.0
grunt release:premajor  # bumps to 1.0.0-rc.0
grunt release           # just an alias for grunt release:minor
```
