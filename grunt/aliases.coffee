module.exports = (grunt) ->

  vagrant: (action) -> grunt.task.run ["vagrant_commands:#{action}"]
  
  release: (action) ->
    bump_action = if action then action else 'minor'
    grunt.task.run [
      "bump-only:#{bump_action}",
      'shell:bump_changelog',
      'bump-commit'
    ]
