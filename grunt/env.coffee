vagrant_ssh_config = ->
  exec = require('child_process').execSync
  key_pattern = /IdentityFile (.*)/
  user_pattern = /User (.*)/
  host_pattern = /HostName (.*)/
  port_pattern = /Port (.*)/
  try
    cmd = 'vagrant ssh-config 2>/dev/null'
    output = exec(cmd).toString()
    key = key_pattern.exec(output)[1]
    user = user_pattern.exec(output)[1]
    host = host_pattern.exec(output)[1]
    port = port_pattern.exec(output)[1]
  catch
    ip = '1.2.3.4'
    key = '~/.ssh/id_rsa'
    user = 'vagrant'
    port = '22'
  { TARGET_HOST: host, TARGET_KEY: key, TARGET_USER: user, TARGET_PORT: port }

module.exports =
  inspec:
    vagrant_ssh_config()
  
  runner:
    VAGRANT_GROUP: 'runner'
    VAGRANT_BOX: 'centos/7'
