# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.3.0 - 2017-08-12
### Added
- Identity service for centralized auth. (#8)
- Foreman server for device management. (#9)

## 0.2.2 - 2017-07-07
### Fixed
- Increase read timeout during CI jobs. (#23)
- Hardcode image name in push jobs. (#24)
- Use devicemapper on CI runner to ensure docker engine starts on CentOS.

## 0.2.1 - 2017-07-07
### Fixed
- Properly parse CI job name during push.

## 0.2.0 - 2017-07-07
### Added
- Kubernetes cluster for container management. (#4)
- NFS server for file sharing. (#5)

## 0.1.0 - 2017-06-19
### Added
- Virtual lab for development and testing. (#2)
- Cockpit for system management. (#3)

## 0.0.1 - 2017-06-17
### Added
- Custom container for running CI jobs efficiently. (#6)
- Grunt tasks for performing common operations. (#1)

